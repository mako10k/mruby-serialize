#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include "mruby.h"
#include "mruby/class.h"
#include "mruby/value.h"
#include "mruby/variable.h"
#include "mruby/data.h"
#include "mruby/range.h"
#include "mruby/string.h"
#include "mruby/array.h"
#include "mruby/error.h"
#include "mruby/hash.h"
#include "mruby/compile.h"
#include "mruby/irep.h"
#include "mruby/dump.h"
#include "mruby/proc.h"

#define SER_UFLAG_MIN 0
#define SER_UFLAG_MAX 7
#define SER_SFLAG_MIN (-4)
#define SER_SFLAG_MAX 3

#define SER_UFLAG_INDIRECT SER_UFLAG_MAX
#define SER_UFLAG_FROZEN (SER_UFLAG_MAX-1)
#define SER_SFLAG_INDIRECT SER_SFLAG_MIN
#define SER_SFLAG_FROZEN (SER_SFLAG_MIN+1)

#define SER_TYPE(t) ((t)&31)
#define SER_UFLAG(t) (((t)>>5)&7)
#define SER_SFLAG(t) (((t)>>5)-(((t)&128)>>4))
#define SER_TYPE_FLAG(t,f) (SER_TYPE(t)|(((f)&7)<<5))
#define SER_UFLAG_FROZEN_P(f) ((f)==SER_UFLAG_FROZEN)
#define SER_UFLAG_FR_DIRECT_P(f) (SER_UFLAG_MIN<=(f)&&(f)<SER_UFLAG_FROZEN)
#define SER_UFLAG_NF_DIRECT_P(f) (SER_UFLAG_MIN<=(f)&&(f)<SER_UFLAG_INDIRECT)
#define SER_SFLAG_FROZEN_P(f) ((f)==SER_UFLAG_FROZEN)
#define SER_SFLAG_FR_DIRECT_P(f) (SER_SFLAG_FROZEN<(f)&&(f)<=SER_SFLAG_MAX)
#define SER_SFLAG_NF_DIRECT_P(f) (SER_SFLAG_INDIRECT<(f)&&(f)<=SER_SFLAG_MAX)
#define SER_FLAG_SET(t,f) ((t)=SER_TYPE_UFLAG((t),(f)))
#define MRB_TT_EX_REF (MRB_TT_MAXDEFINE + 0)
#define MRB_TT_EX_NIL (MRB_TT_MAXDEFINE + 1)
#define MRB_TT_EX_RANGE (MRB_TT_MAXDEFINE + 2)
#define MRB_TT_EX_TOP (MRB_TT_MAXDEFINE + 3)

#define SER_DEBUG (1<<0)
#define SER_MEM (1<<1)
#define SER_WRITE (1<<2)

struct serialize_ctx
{
  FILE *fp;
  char *buf;
  size_t size;
  size_t offset;
  mrb_value list;
  mrb_int flags;
  mrb_int nest;
};

static inline void
write_raw (mrb_state * mrb, struct serialize_ctx *ctx,
	   void const *buf, size_t len)
{
  size_t wlen;

  if (ctx->flags & SER_MEM)
    {
      if (ctx->offset + len > ctx->size)
	{
	  size_t newsize;
	  char *newbuf;

	  if (ctx->size == 0)
	    {
	      newsize = 16;
	      while (ctx->offset + len > newsize)
		newsize *= 2;
	      newbuf = malloc (newsize);
	      if (newbuf == NULL)
		mrb_sys_fail (mrb, "malloc");
	    }
	  else
	    {
	      newsize = ctx->size * 2;
	      while (ctx->offset + len > newsize)
		newsize *= 2;
	      newbuf = realloc (ctx->buf, newsize);
	      if (newbuf == NULL)
		mrb_sys_fail (mrb, "realloc");
	    }
	  ctx->size = newsize;
	  ctx->buf = newbuf;
	}
      memcpy (ctx->buf + ctx->offset, buf, len);
      ctx->offset += len;
      return;
    }
  wlen = fwrite (buf, 1, len, ctx->fp);
  if (wlen < len)
    mrb_sys_fail (mrb, "fwrite");
}

static inline void
open_raw (mrb_state * mrb, struct serialize_ctx *ctx, mrb_value list,
	  char *buf, size_t size, size_t offset, mrb_int flags, mrb_int nest)
{
  ctx->list = list;
  ctx->flags = flags;
  ctx->nest = nest;
  if (flags & SER_WRITE)
    {
      ctx->buf = NULL;
      ctx->size = 0;
      ctx->offset = 0;
      if (flags & SER_MEM)
	ctx->fp = NULL;
      else
	ctx->fp = open_memstream (&ctx->buf, &ctx->size);
      write_raw (mrb, ctx, buf, offset);
    }
  else
    {
      ctx->buf = buf;
      ctx->size = size;
      ctx->offset = offset;
      if (flags & SER_MEM)
	ctx->fp = NULL;
      else
	ctx->fp = fmemopen (buf + offset, size - offset, "r");
    }
}

static inline void *
mrb_serialize_ctx_ptr (mrb_state * mrb, struct serialize_ctx *ctx)
{
  if (ctx->flags & SER_MEM)
    return ctx->buf;
  if (fflush (ctx->fp) == EOF)
    mrb_sys_fail (mrb, "fflush");
  return ctx->buf;
}

static inline size_t
mrb_serialize_ctx_len (mrb_state * mrb, struct serialize_ctx *ctx)
{
  if (ctx->flags & SER_MEM)
    return ctx->offset;
  if (fflush (ctx->fp) == EOF)
    mrb_sys_fail (mrb, "fflush");
  return ftell (ctx->fp) + ctx->offset;
}

static inline void
close_raw (mrb_state * mrb, struct serialize_ctx *ctx)
{
  if (!(ctx->flags & SER_MEM))
    if (fclose (ctx->fp) == EOF)
      mrb_sys_fail (mrb, "fclose");
  if ((ctx->flags & SER_WRITE) && ctx->size > 0)
    free (ctx->buf);
}

static inline FILE *
mrb_serialize_ctx_fp (mrb_state * mrb, struct serialize_ctx *ctx)
{
  if (ctx->flags & SER_MEM)
    {
      struct serialize_ctx c;

      c = *ctx;
      open_raw (mrb, ctx, c.list, c.buf, c.size, c.offset, c.flags & ~SER_MEM,
		c.nest);
      close_raw (mrb, &c);
    }
  return ctx->fp;
}

#define debug_print_func(ctx, pos) \
  do {\
    if ((ctx)->flags & SER_DEBUG)\
      {\
        debug_print_indent (ctx);\
        fprintf (stderr, "-- %s [%s]\n", __func__, (pos));\
      }\
  } while(0)

static inline void
debug_print_indent (struct serialize_ctx *ctx)
{
  int i;

  for (i = 0; i < ctx->nest * 2; i++)
    fputc (' ', stderr);
}

#define debug_print_func_start(ctx) \
  do {\
    debug_print_func ((ctx), "start");\
    (ctx)->nest ++;\
  } while (0)

#define debug_print_func_end(ctx) \
  do {\
    --(ctx)->nest;\
    debug_print_func ((ctx), "end");\
  } while (0)

static inline void
debug_print_type (struct serialize_ctx *ctx, mrb_int type)
{
  FILE *const fp = stderr;
  const static char const *typenames[] = {
    "false",
    "free",
    "true",
    "fixnum",
    "symbol",
    "undef",
    "float",
    "cptr",
    "object",
    "class",
    "module",
    "iclass",
    "sclass",
    "proc",
    "array",
    "hash",
    "string",
    "range(excl=false)",
    "exception",
    "file",
    "env",
    "data",
    "fiber",
    "istruct",
    "break",
    "ref",
    "nil",
    "range(excl=true)",
    "main"
  };

  switch (SER_TYPE (type))
    {
    case MRB_TT_FALSE:
    case MRB_TT_FREE:
    case MRB_TT_TRUE:
    case MRB_TT_UNDEF:
    case MRB_TT_FLOAT:
    case MRB_TT_CPTR:
    case MRB_TT_CLASS:
    case MRB_TT_MODULE:
    case MRB_TT_ICLASS:
    case MRB_TT_SCLASS:
    case MRB_TT_FILE:
    case MRB_TT_ENV:
    case MRB_TT_DATA:
    case MRB_TT_FIBER:
    case MRB_TT_ISTRUCT:
    case MRB_TT_BREAK:
    case MRB_TT_EX_NIL:
    case MRB_TT_EX_TOP:
      fprintf (fp, "%s", typenames[SER_TYPE (type)]);
      break;

    case MRB_TT_FIXNUM:
      fprintf (fp, "%s", typenames[SER_TYPE (type)]);
      if (SER_SFLAG_NF_DIRECT_P (SER_SFLAG (type)))
	fprintf (fp, "(val=%" MRB_PRId ")", SER_SFLAG (type));
      break;

    case MRB_TT_SYMBOL:
      fprintf (fp, "%s", typenames[SER_TYPE (type)]);
      if (SER_UFLAG_NF_DIRECT_P (SER_UFLAG (type)))
	fprintf (fp, "(size=%" MRB_PRId ")", SER_UFLAG (type));
      break;

    case MRB_TT_OBJECT:
    case MRB_TT_PROC:
    case MRB_TT_RANGE:
    case MRB_TT_EXCEPTION:
    case MRB_TT_EX_RANGE:
      fprintf (fp, "%s", typenames[SER_TYPE (type)]);
      if (SER_UFLAG_FROZEN_P (SER_UFLAG (type)))
	fprintf (fp, "(frozen)");
      break;

    case MRB_TT_ARRAY:
    case MRB_TT_HASH:
      fprintf (fp, "%s", typenames[SER_TYPE (type)]);
      if (SER_UFLAG_FR_DIRECT_P (SER_UFLAG (type)))
	fprintf (fp, "(size=%" MRB_PRId ")", SER_UFLAG (type));
      if (SER_UFLAG_FROZEN_P (SER_UFLAG (type)))
	fprintf (fp, "(frozen)");
      break;

    case MRB_TT_STRING:
      fprintf (fp, "%s", typenames[SER_TYPE (type)]);
      if (SER_UFLAG_FR_DIRECT_P (SER_UFLAG (type)))
	fprintf (fp, "(size=%" MRB_PRId ")", SER_UFLAG (type));
      if (SER_UFLAG_FROZEN_P (SER_UFLAG (type)))
	fprintf (fp, "(frozen)");
      break;

    case MRB_TT_EX_REF:
      fprintf (fp, "%s", typenames[SER_TYPE (type)]);
      if (SER_SFLAG_NF_DIRECT_P (SER_SFLAG (type)))
	fprintf (fp, "(ref=%" MRB_PRId ")", SER_SFLAG (type));
      break;

    default:
      fprintf (fp, "unknown(type=%" MRB_PRId ",flag=%" MRB_PRId ")",
	       SER_TYPE (type), SER_UFLAG (type));
      break;
    }
  fputc ('\n', fp);
}

static inline void
debug_print_string (FILE * fp, char const *buf, size_t len)
{
  int i;

  fputc ('"', fp);
  for (i = 0; i < len; i++)
    {
      if (buf[i] == '\\' || buf[i] == '"')
	fputc ('\\', fp);
      if (isgraph (buf[i]) || isspace (buf[i]))
	fputc (buf[i], fp);
      else
	fprintf (fp, "\\x%02x", buf[i]);
    }
  fputc ('"', fp);
  fputc ('\n', fp);
}

static inline int
putc_raw (mrb_state * mrb, struct serialize_ctx *ctx, int c)
{
  if (ctx->flags & SER_MEM)
    {
      if (ctx->offset >= ctx->size)
	{
	  size_t newsize;
	  char *newbuf;

	  if (ctx->size == 0)
	    {
	      newsize = 16;
	      newbuf = malloc (newsize);
	      if (newbuf == NULL)
		mrb_sys_fail (mrb, "malloc");
	    }
	  else
	    {
	      newsize = ctx->size * 2;
	      newbuf = realloc (ctx->buf, newsize);
	      if (newbuf == NULL)
		mrb_sys_fail (mrb, "realloc");
	    }
	  ctx->size = newsize;
	  ctx->buf = newbuf;
	}
      ctx->buf[ctx->offset++] = c;
      return c & 255;
    }
  c = fputc (c, ctx->fp);
  if (c == EOF)
    mrb_sys_fail (mrb, "fputc");
  return c;
}

static inline int
getc_raw (mrb_state * mrb, struct serialize_ctx *ctx)
{
  int c;

  if (ctx->flags & SER_MEM)
    {
      if (ctx->offset >= ctx->size)
	mrb_raise (mrb, E_RUNTIME_ERROR,
		   "serialize format error (Unexpected EOF)");
      return ctx->buf[ctx->offset++] & 255;
    }
  c = fgetc (ctx->fp);
  if (c == EOF)
    mrb_sys_fail (mrb, "serialize format error (Unexpected EOF)");
  return c;
}

static inline void
read_raw (mrb_state * mrb, struct serialize_ctx *ctx, void *buf, size_t len)
{
  size_t rlen;

  if (ctx->flags & SER_MEM)
    {
      if (ctx->offset + len > ctx->size)
	mrb_raise (mrb, E_RUNTIME_ERROR,
		   "serialize format error (Unexpected EOF)");
      memcpy (buf, ctx->buf + ctx->offset, len);
      ctx->offset += len;
      return;
    }
  rlen = fread (buf, 1, len, ctx->fp);
  if (rlen < len)
    {
      if (feof (ctx->fp))
	mrb_raise (mrb, E_RUNTIME_ERROR,
		   "serialize format error (Unexpected EOF)");
      mrb_sys_fail (mrb, "fread");
    }
}

static inline void
register_value (mrb_state * mrb, struct serialize_ctx *ctx, mrb_value value)
{
  if (ctx->flags & SER_DEBUG)
    {
      debug_print_indent (ctx);
      fprintf (stderr, "** set register[%" MRB_PRId "] = ",
	       RARRAY_LEN (ctx->list));
      if (mrb_immediate_p (value))
	mrb_funcall (mrb, mrb_obj_value (mrb->top_self), "p", 1, value);
      else
	{
	  fprintf (stderr, "0x%p, type = ", mrb_obj_ptr (value));
	  debug_print_type (ctx, mrb_type (value));
	}
    }
  mrb_ary_push (mrb, ctx->list, value);
}

static inline mrb_value
get_registered_value (mrb_state * mrb, struct serialize_ctx *ctx, mrb_int ref)
{
  mrb_value *listptr;
  mrb_int listlen;
  listptr = RARRAY_PTR (ctx->list);
  listlen = RARRAY_LEN (ctx->list);
  if (ref < 0)
    ref += listlen;
  if (ref >= listlen)
    mrb_raise (mrb, E_RUNTIME_ERROR, "serialize format error (invalid ref)");
  if (ctx->flags & SER_DEBUG)
    {
      debug_print_indent (ctx);
      fprintf (stderr, "** get register[%" MRB_PRId "] = ", ref);
      if (mrb_immediate_p (listptr[ref]))
	mrb_funcall (mrb, mrb_obj_value (mrb->top_self), "p", 1,
		     listptr[ref]);
      else
	{
	  fprintf (stderr, "0x%p, type = ", mrb_obj_ptr (listptr[ref]));
	  debug_print_type (ctx, mrb_type (listptr[ref]));
	}
    }
  return listptr[ref];
}

static inline void
write_intval (mrb_state * mrb, struct serialize_ctx *ctx, int64_t val)
{
  int cont;
  if (val >= 0)
    do
      {
	cont = val >= 64;
	putc_raw (mrb, ctx, (val & 127) | (cont ? 128 : 0));
	val >>= 7;
      }
    while (cont);
  else
    do
      {
	cont = val < -64;
	putc_raw (mrb, ctx, (val & 127) | (cont ? 128 : 0));
	// if bit shift is arithmetic : ~(-1 >> 7) == 2#000000000..
	// if bit shift is logical    : ~(-1 >> 7) == 2#111111100..
	val = (val >> 7) | ~((mrb_int) - 1 >> 7);
      }
    while (cont);
}

static inline void
write_uintval (mrb_state * mrb, struct serialize_ctx *ctx, uint64_t val)
{
  int cont;
  do
    {
      cont = val >= 128;
      putc_raw (mrb, ctx, (val & 127) | (cont ? 128 : 0));
      val >>= 7;
    }
  while (cont);
}

static inline int64_t
read_intval (mrb_state * mrb, struct serialize_ctx *ctx)
{
  int64_t ret = 0;
  int bit = 0;
  int c;
  do
    {
      c = getc_raw (mrb, ctx);
      ret |= (c & 127) << bit;
      bit += 7;
    }
  while (c & 0x80);
  if (c & 0x40)
    ret |= (-1) << bit;
  return ret;
}

static inline uint64_t
read_uintval (mrb_state * mrb, struct serialize_ctx *ctx)
{
  uint64_t ret = 0;
  int bit = 0;
  int c;
  do
    {
      c = getc_raw (mrb, ctx);
      ret |= (c & 127) << bit;
      bit += 7;
    }
  while (c & 0x80);
  return ret;
}

static inline void
write_strval (mrb_state * mrb, struct serialize_ctx *ctx,
	      char const *str, size_t len)
{
  write_uintval (mrb, ctx, len);
  write_raw (mrb, ctx, str, len);
  if (ctx->flags & SER_DEBUG)
    {
      debug_print_indent (ctx);
      fprintf (stderr, "** write strval = ");
      debug_print_string (stderr, str, len);
    }
}

static inline void
write_type (mrb_state * mrb, struct serialize_ctx *ctx, mrb_int type)
{
  if (ctx->flags & SER_DEBUG)
    {
      debug_print_indent (ctx);
      fprintf (stderr, "** write type = ");
      debug_print_type (ctx, type);
    }
  putc_raw (mrb, ctx, type);
}

static inline mrb_int
read_type (mrb_state * mrb, struct serialize_ctx *ctx)
{
  mrb_int type;
  type = getc_raw (mrb, ctx);
  if (ctx->flags & SER_DEBUG)
    {
      debug_print_indent (ctx);
      fprintf (stderr, "** read type = ");
      debug_print_type (ctx, type);
    }
  return type;
}

static inline void
write_fixnum (mrb_state * mrb, struct serialize_ctx *ctx, mrb_int value)
{
  debug_print_func_start (ctx);
  if (-4 < value && value < 4)
    {
      write_type (mrb, ctx, SER_TYPE_FLAG (MRB_TT_FIXNUM, value));
      debug_print_func_end (ctx);
      return;
    }
  write_type (mrb, ctx, SER_TYPE_FLAG (MRB_TT_FIXNUM, SER_SFLAG_INDIRECT));
  write_intval (mrb, ctx, value);
  if (value < -32 || 32 <= value)
    register_value (mrb, ctx, mrb_fixnum_value (value));
  debug_print_func_end (ctx);
}

static inline mrb_int
read_fixnum (mrb_state * mrb, struct serialize_ctx *ctx, mrb_int flag)
{
  mrb_int ret;
  debug_print_func_start (ctx);
  if (flag == SER_SFLAG_INDIRECT)
    {
      ret = read_intval (mrb, ctx);
      if (ret < -32 || 32 <= ret)
	register_value (mrb, ctx, mrb_fixnum_value (ret));
      debug_print_func_end (ctx);
      return ret;
    }
  debug_print_func_end (ctx);
  return flag;
}

static inline void
write_symbol (mrb_state * mrb, struct serialize_ctx *ctx, mrb_sym sym)
{
  char const *symname;
  mrb_int len;
  debug_print_func_start (ctx);
  symname = mrb_sym2name_len (mrb, sym, &len);
  if (len > 0)
    register_value (mrb, ctx, mrb_symbol_value (sym));
  if (SER_UFLAG_NF_DIRECT_P (len))
    write_type (mrb, ctx, SER_TYPE_FLAG (MRB_TT_SYMBOL, len));
  else
    {
      write_type (mrb, ctx,
		  SER_TYPE_FLAG (MRB_TT_SYMBOL, SER_UFLAG_INDIRECT));
      write_uintval (mrb, ctx, len);
    }
  if (ctx->flags & SER_DEBUG)
    {
      debug_print_indent (ctx);
      fprintf (stderr, "** write symbol = ");
      debug_print_string (stderr, symname, len);
    }
  write_raw (mrb, ctx, symname, len);
  debug_print_func_end (ctx);
}

static inline mrb_sym
read_symbol (mrb_state * mrb, struct serialize_ctx *ctx, mrb_int flag)
{
  char *buf;
  size_t len;
  mrb_sym sym;
  debug_print_func_start (ctx);
  if (SER_UFLAG_NF_DIRECT_P (flag))
    len = flag;
  else
    len = read_uintval (mrb, ctx);
  buf = alloca (len);
  read_raw (mrb, ctx, buf, len);
  if (ctx->flags & SER_DEBUG)
    {
      debug_print_indent (ctx);
      fprintf (stderr, "** read symbol = ");
      debug_print_string (stderr, buf, len);
    }
  sym = mrb_intern (mrb, buf, len);
  if (len > 0)
    register_value (mrb, ctx, mrb_symbol_value (sym));
  debug_print_func_end (ctx);
  return sym;
}

static inline void
write_float (mrb_state * mrb, struct serialize_ctx *ctx, mrb_float value)
{
  debug_print_func_start (ctx);
  register_value (mrb, ctx, mrb_float_value (mrb, value));
  write_type (mrb, ctx, MRB_TT_FLOAT);
  write_raw (mrb, ctx, &value, sizeof (value));
  debug_print_func_end (ctx);
}

static inline mrb_float
read_float (mrb_state * mrb, struct serialize_ctx *ctx)
{
  mrb_float ret;
  debug_print_func_start (ctx);
  read_raw (mrb, ctx, &ret, sizeof (ret));
  if (ctx->flags & SER_DEBUG)
    {
      debug_print_indent (ctx);
      fprintf (stderr, "read float = %f\n", ret);
    }
  register_value (mrb, ctx, mrb_float_value (mrb, ret));
  debug_print_func_end (ctx);
  return ret;
}

static inline void
write_value (mrb_state * mrb, struct serialize_ctx *ctx, mrb_value value);
static inline void
write_string (mrb_state * mrb, struct serialize_ctx *ctx, struct RString *str)
{
  mrb_int len;
  debug_print_func_start (ctx);
  len = RSTR_LEN (str);
  if (len > 0)
    register_value (mrb, ctx, mrb_obj_value (str));
  if (SER_UFLAG_FR_DIRECT_P (len) && !MRB_FROZEN_P (str))
    write_type (mrb, ctx, SER_TYPE_FLAG (MRB_TT_STRING, len));
  else
    {
      write_type (mrb, ctx,
		  SER_TYPE_FLAG (MRB_TT_STRING,
				 MRB_FROZEN_P (str) ? SER_UFLAG_FROZEN :
				 SER_UFLAG_INDIRECT));
      write_uintval (mrb, ctx, len);
    }
  if (ctx->flags & SER_DEBUG)
    {
      fprintf (stderr, "write raw (%" MRB_PRId ") bytes: ", len);
      debug_print_string (stderr, RSTR_PTR (str), len);
    }
  write_raw (mrb, ctx, RSTR_PTR (str), len);
  debug_print_func_end (ctx);
}

static inline struct RString *
read_string (mrb_state * mrb, struct serialize_ctx *ctx, mrb_int flag)
{
  mrb_value strval;
  char *buf;
  mrb_int len;
  debug_print_func_start (ctx);
  if (flag >= 6)
    len = read_uintval (mrb, ctx);
  else
    len = flag;
  buf = alloca (len);
  read_raw (mrb, ctx, buf, len);
  if (ctx->flags & SER_DEBUG)
    {
      fprintf (stderr, "read raw (%" MRB_PRId ") bytes: ", len);
      debug_print_string (stderr, buf, len);
    }
  strval = mrb_str_new (mrb, buf, len);
  if (SER_UFLAG_FROZEN_P (flag))
    MRB_SET_FROZEN_FLAG (mrb_str_ptr (strval));
  if (len > 0)
    register_value (mrb, ctx, strval);
  debug_print_func_end (ctx);
  return mrb_str_ptr (strval);
}

static inline void
write_class_tt (mrb_state * mrb, struct serialize_ctx *ctx,
		struct RClass *cls, mrb_int type)
{
  mrb_int ai;
  mrb_value clsnameval;
  mrb_int i;
  struct RClass **beg, **end;
  beg = &mrb->object_class;
  end = &mrb->kernel_module;
  for (i = 0; i < end - beg + 1; i++)
    if (cls == beg[i])
      {
	write_type (mrb, ctx, type);
	write_intval (mrb, ctx, -i - 1);
	return;
      }
  register_value (mrb, ctx, mrb_obj_value (cls));
  write_type (mrb, ctx, type);
  ai = mrb_gc_arena_save (mrb);
  clsnameval = mrb_class_path (mrb, cls);
  write_strval (mrb, ctx, RSTRING_PTR (clsnameval), RSTRING_LEN (clsnameval));
  mrb_gc_arena_restore (mrb, ai);
}

static inline void
write_class (mrb_state * mrb, struct serialize_ctx *ctx, struct RClass *cls)
{
  debug_print_func_start (ctx);
  write_class_tt (mrb, ctx, cls, MRB_TT_CLASS);
  debug_print_func_end (ctx);
}

static inline void
write_module (mrb_state * mrb, struct serialize_ctx *ctx, struct RClass *cls)
{
  debug_print_func_start (ctx);
  write_class_tt (mrb, ctx, cls, MRB_TT_MODULE);
  debug_print_func_end (ctx);
}

static inline void
write_sclass (mrb_state * mrb, struct serialize_ctx *ctx, struct RClass *cls)
{
  debug_print_func_start (ctx);
  write_type (mrb, ctx, MRB_TT_SCLASS);
  write_value (mrb, ctx,
	       mrb_obj_iv_get (mrb, (struct RObject *) cls,
			       mrb_intern_lit (mrb, "__attached__")));
  debug_print_func_end (ctx);
}

static inline struct RClass *
get_class (mrb_state * mrb, char const *name, mrb_int len)
{
  struct RClass *ret = mrb->object_class;
  char const *p, *beg, *end;
  mrb_value cv;
  mrb_sym cnsym;
  beg = name;
  end = name + len;
  for (;;)
    {
      p = beg;
      while ((p < end && p[0] != ':') || (p + 1 < end && p[1] != ':'))
	p++;
      cnsym = mrb_intern (mrb, beg, p - beg);
      if (!mrb_mod_cv_defined (mrb, ret, cnsym))
	mrb_raisef (mrb, E_ARGUMENT_ERROR, "undefined class or module %S",
		    mrb_str_new (mrb, name, p - name));
      cv = mrb_mod_cv_get (mrb, ret, cnsym);
      if (mrb_type (cv) != MRB_TT_CLASS && mrb_type (cv) != MRB_TT_MODULE)
	mrb_raisef (mrb,
		    E_TYPE_ERROR,
		    "%S is not class or module",
		    mrb_str_new (mrb, name, p - name));
      ret = mrb_class_ptr (cv);
      if (p >= end)
	break;
      beg = p + 2;
    }
  return ret;
}

static inline struct RClass *
read_class (mrb_state * mrb, struct serialize_ctx *ctx)
{
  mrb_int len;
  char *name;
  struct RClass *ret;
  struct RClass **beg;
  debug_print_func_start (ctx);
  beg = &mrb->object_class;
  len = read_intval (mrb, ctx);
  if (len < 0)
    {
      debug_print_func_end (ctx);
      return beg[-len - 1];
    }
  name = alloca (len);
  read_raw (mrb, ctx, name, len);
  if (ctx->flags & SER_DEBUG)
    {
      fprintf (stderr, "read raw (%" MRB_PRId ") bytes: ", len);
      debug_print_string (stderr, name, len);
    }
  ret = get_class (mrb, name, len);
  if (ret == NULL)
    {
      mrb_value nameval;
      nameval = mrb_str_new (mrb, name, len);
      mrb_raisef (mrb, E_RUNTIME_ERROR, "cannot find class %S", nameval);
    }
  register_value (mrb, ctx, mrb_obj_value (ret));
  debug_print_func_end (ctx);
  return ret;
}

static inline void
write_object (mrb_state * mrb, struct serialize_ctx *ctx, struct RObject *obj)
{
  mrb_int i, ai, arylen;
  mrb_value ary, *aryptr;
  debug_print_func_start (ctx);
  register_value (mrb, ctx, mrb_obj_value (obj));
  write_type (mrb, ctx, obj->tt | ((MRB_FROZEN_P (obj) ? 6 : 7) << 5));
  if (obj->c)
    write_value (mrb, ctx, mrb_obj_value (obj->c));
  else
    write_value (mrb, ctx, mrb_nil_value ());
  ai = mrb_gc_arena_save (mrb);
  ary = mrb_obj_instance_variables (mrb, mrb_obj_value (obj));
  aryptr = RARRAY_PTR (ary);
  arylen = RARRAY_LEN (ary);
  write_uintval (mrb, ctx, arylen);
  for (i = 0; i < arylen; i++)
    {
      mrb_sym ivsym;
      ivsym = mrb_symbol (aryptr[i]);
      write_value (mrb, ctx, aryptr[i]);
      write_value (mrb, ctx, mrb_obj_iv_get (mrb, obj, ivsym));
    }
  mrb_gc_arena_restore (mrb, ai);
  debug_print_func_end (ctx);
}

static inline void
write_exception (mrb_state * mrb, struct serialize_ctx *ctx,
		 struct RObject *obj)
{
  debug_print_func_start (ctx);
  write_object (mrb, ctx, obj);
  write_value (mrb, ctx,
	       mrb_obj_iv_get (mrb, obj, mrb_intern_lit (mrb, "mesg")));
  debug_print_func_end (ctx);
}

static inline mrb_value
read_value (mrb_state * mrb, struct serialize_ctx *ctx);
static inline struct RObject *
read_object_tt (mrb_state * mrb,
		struct serialize_ctx *ctx, enum mrb_vtype type)
{
  struct RObject *ret;
  mrb_int ivlen, i;
  mrb_value clsval;
  ret = (struct RObject *) mrb_obj_alloc (mrb, type, NULL);
  register_value (mrb, ctx, mrb_obj_value (ret));
  clsval = read_value (mrb, ctx);
  if (mrb_nil_p (clsval))
    ret->c = NULL;
  else
    ret->c = mrb_class_ptr (clsval);
  ivlen = read_uintval (mrb, ctx);
  for (i = 0; i < ivlen; i++)
    {
      mrb_sym ivname;
      mrb_value ivvalue;
      ivname = mrb_symbol (read_value (mrb, ctx));
      ivvalue = read_value (mrb, ctx);
      mrb_obj_iv_set (mrb, ret, ivname, ivvalue);
    }
  return ret;
}

static inline struct RObject *
read_object (mrb_state * mrb, struct serialize_ctx *ctx, mrb_int flag)
{
  struct RObject *ret;
  debug_print_func_start (ctx);
  ret = read_object_tt (mrb, ctx, MRB_TT_OBJECT);
  if (SER_UFLAG_FROZEN_P (flag))
    MRB_SET_FROZEN_FLAG (ret);
  debug_print_func_end (ctx);
  return ret;
}

static inline struct RException *
read_exception (mrb_state * mrb, struct serialize_ctx *ctx, mrb_int flag)
{
  struct RException *ret;
  debug_print_func_start (ctx);
  ret = (struct RException *) read_object_tt (mrb, ctx, MRB_TT_EXCEPTION);
  mrb_obj_iv_set (mrb, (struct RObject *) ret,
		  mrb_intern_lit (mrb, "mesg"), read_value (mrb, ctx));
  if (SER_UFLAG_FROZEN_P (flag))
    MRB_SET_FROZEN_FLAG (ret);
  debug_print_func_end (ctx);
  return ret;
}

static inline mrb_value
write_ref (mrb_state * mrb, struct serialize_ctx *ctx, mrb_value value)
{
  mrb_value *listptr;
  mrb_int listlen, i;
  listptr = RARRAY_PTR (ctx->list);
  listlen = RARRAY_LEN (ctx->list);
  for (i = 0; i < listlen; i++)
    if (mrb_obj_eq (mrb, value, listptr[i]))
      {
	debug_print_func_start (ctx);
	if (i * 2 >= listlen)
	  i -= listlen;
	if (-4 < i && i < 4)
	  write_type (mrb, ctx, SER_TYPE_FLAG (MRB_TT_EX_REF, i));
	else
	  {
	    write_type (mrb, ctx,
			SER_TYPE_FLAG (MRB_TT_EX_REF, SER_SFLAG_INDIRECT));
	    write_intval (mrb, ctx, i);
	  }
	if (ctx->flags & SER_DEBUG)
	  {
	    debug_print_indent (ctx);
	    if (i < 0)
	      fprintf (stderr,
		       "** write ref[%" MRB_PRId "/*%" MRB_PRId "*/] = ", i,
		       i + listlen);
	    else
	      fprintf (stderr, "** write ref[%" MRB_PRId "] = ", i);
	    if (mrb_immediate_p (value))
	      mrb_funcall (mrb, mrb_obj_value (mrb->top_self), "p", 1, value);
	    else
	      {
		fprintf (stderr, "0x%p, type = ", mrb_obj_ptr (value));
		debug_print_type (ctx, mrb_type (value));
	      }
	  }
	debug_print_func_end (ctx);
	return mrb_fixnum_value (i);
      }
  return mrb_nil_value ();
}

static inline mrb_value
read_ref (mrb_state * mrb, struct serialize_ctx *ctx, mrb_int flag)
{
  mrb_int i;
  if (SER_SFLAG_NF_DIRECT_P (flag))
    i = flag;
  else
    i = read_intval (mrb, ctx);
  return get_registered_value (mrb, ctx, i);
}

static inline void
write_obj_value (mrb_state * mrb, struct serialize_ctx *ctx,
		 struct RObject *o)
{
  if (o == NULL)
    write_value (mrb, ctx, mrb_nil_value ());
  else
    write_value (mrb, ctx, mrb_obj_value (o));
}

static inline struct RObject *
read_obj_value (mrb_state * mrb, struct serialize_ctx *ctx)
{
  mrb_value ret;

  ret = read_value (mrb, ctx);
  if (mrb_nil_p (ret))
    return NULL;
  return mrb_obj_ptr (ret);
}

static inline void
write_proc (mrb_state * mrb, struct serialize_ctx *ctx, struct RProc *p)
{
  FILE *fp;

  debug_print_func_start (ctx);
  if (MRB_PROC_CFUNC_P (p))
    mrb_raise (mrb, E_RUNTIME_ERROR, "cannot serialize cfunc proc");
  register_value (mrb, ctx, mrb_obj_value (p));
  write_type (mrb, ctx, MRB_TT_PROC);
  if (ctx->flags & SER_DEBUG)
    fprintf (stderr, "write proc.flags\n");

  // flags の書き込み
  write_intval (mrb, ctx, p->flags);

  // irep の書き込み
  if (ctx->flags & SER_DEBUG)
    fprintf (stderr, "write proc.irep\n");
  fp = mrb_serialize_ctx_fp (mrb, ctx);
  mrb_dump_irep_binary (mrb, p->body.irep, 0, fp);

  // upper の書き込み
  if (ctx->flags & SER_DEBUG)
    fprintf (stderr, "write proc.upper\n");
  write_obj_value (mrb, ctx, (struct RObject *) p->upper);

  // Env or Target Class の書き込み
  if (MRB_PROC_ENV_P (p))
    {
      if (ctx->flags & SER_DEBUG)
	fprintf (stderr, "write proc.env\n");
      write_value (mrb, ctx, mrb_obj_value (p->e.env));
    }
  else
    {
      if (ctx->flags & SER_DEBUG)
	fprintf (stderr, "write proc.target_class\n");
      write_value (mrb, ctx, mrb_obj_value (p->e.target_class));
    }

  debug_print_func_end (ctx);
}

static inline struct RProc *
read_proc (mrb_state * mrb, struct serialize_ctx *ctx)
{
  mrb_int flags;
  struct RProc *p;
  FILE *fp;
  struct mrb_irep *irep;

  debug_print_func_start (ctx);

  // Flag
  flags = read_intval (mrb, ctx);	// must check flags...

  // Irep
  fp = mrb_serialize_ctx_fp (mrb, ctx);
  irep = mrb_read_irep_file (mrb, fp);
  if (irep == NULL)
    mrb_raise (mrb, E_RUNTIME_ERROR,
	       "invalid serialize format (malformed irep data)");

  // Preregister Proc
  p = mrb_proc_new (mrb, irep);
  mrb_irep_decref (mrb, p->body.irep);
  register_value (mrb, ctx, mrb_obj_value (p));
  p->flags = flags;

  // Upper Proc
  p->upper = (struct RProc *) read_obj_value (mrb, ctx);
  if (p->upper && p->upper->tt != MRB_TT_PROC)
    mrb_raise (mrb, E_RUNTIME_ERROR,
	       "invalid serialize format (unexpected value for upper proc)");
  mrb_field_write_barrier (mrb, (struct RBasic *) p,
			   (struct RBasic *) p->upper);

  // Env or Target Class
  if (MRB_PROC_ENV_P (p))
    {
      mrb_value env;
      struct REnv *e;

      env = read_value (mrb, ctx);
      if (mrb_type (env) != MRB_TT_ENV)
	mrb_raise (mrb, E_RUNTIME_ERROR,
		   "invalid serialize format (unexpected type for env of proc)");
      e = (struct REnv *) mrb_obj_ptr (env);
      p->e.env = e;
      mrb_field_write_barrier (mrb, (struct RBasic *) p, (struct RBasic *) e);
    }
  else
    {
      struct RClass *tc;

      tc = (struct RClass *) read_obj_value (mrb, ctx);
      if (tc->tt != MRB_TT_CLASS && tc->tt != MRB_TT_MODULE
	  && tc->tt != MRB_TT_EXCEPTION)
	mrb_raise (mrb, E_RUNTIME_ERROR,
		   "invalid serialize format (unexpected type for target class of proc)");
      p->e.target_class = tc;
      mrb_field_write_barrier (mrb, (struct RBasic *) p,
			       (struct RBasic *) tc);
    }

  debug_print_func_end (ctx);
  return p;
}

static inline void
write_env (mrb_state * mrb, struct serialize_ctx *ctx, struct REnv *e)
{
  mrb_int len, i;
  debug_print_func_start (ctx);
  register_value (mrb, ctx, mrb_obj_value (e));
  write_type (mrb, ctx, MRB_TT_ENV);
  write_intval (mrb, ctx, e->flags & ~MRB_ENV_STACK_UNSHARED);
  write_value (mrb, ctx, mrb_obj_value (e->c));
  len = MRB_ENV_STACK_LEN (e);
  for (i = 0; i < len; i++)
    write_value (mrb, ctx, e->stack[i]);
  write_value (mrb, ctx, mrb_symbol_value (e->mid));
  debug_print_func_end (ctx);
}

static inline struct REnv *
read_env (mrb_state * mrb, struct serialize_ctx *ctx)
{
  struct REnv *e;
  mrb_int len, i;
  mrb_value cval;
  debug_print_func_start (ctx);
  e = (struct REnv *) mrb_obj_alloc (mrb, MRB_TT_ENV, NULL);
  register_value (mrb, ctx, mrb_obj_value (e));
  e->flags = read_intval (mrb, ctx) & ~MRB_ENV_STACK_UNSHARED;
  cval = read_value (mrb, ctx);
  e->c = mrb_class_ptr (cval);
  mrb_field_write_barrier (mrb, (struct RBasic *) e, (struct RBasic *) e->c);
  len = MRB_ENV_STACK_LEN (e);
  e->stack = mrb_malloc (mrb, sizeof (mrb_value) * len);
  MRB_ENV_UNSHARE_STACK (e);
  for (i = 0; i < len; i++)
    e->stack[i] = read_value (mrb, ctx);
  e->cxt = mrb->c;
  e->mid = mrb_symbol (read_value (mrb, ctx));
  debug_print_func_end (ctx);
  return e;
}

static inline void
write_array (mrb_state * mrb, struct serialize_ctx *ctx, struct RArray *a)
{
  mrb_value *ptr;
  mrb_int len, i;
  debug_print_func_start (ctx);
  register_value (mrb, ctx, mrb_obj_value (a));
  ptr = ARY_PTR (a);
  len = ARY_LEN (a);
  if (SER_UFLAG_FR_DIRECT_P (len) && !MRB_FROZEN_P (a))
    write_type (mrb, ctx, SER_TYPE_FLAG (MRB_TT_ARRAY, len));
  else
    {
      write_type (mrb, ctx,
		  SER_TYPE_FLAG (MRB_TT_ARRAY,
				 MRB_FROZEN_P (a) ? SER_UFLAG_FROZEN :
				 SER_UFLAG_INDIRECT));
      write_uintval (mrb, ctx, len);
    }
  for (i = 0; i < len; i++)
    write_value (mrb, ctx, ptr[i]);
  debug_print_func_end (ctx);
}

static inline struct RArray *
read_array (mrb_state * mrb, struct serialize_ctx *ctx, mrb_int flag)
{
  mrb_int len, ai, i;
  mrb_value *ptr, aryval;
  struct RArray *ret;
  debug_print_func_start (ctx);
  if (SER_UFLAG_FR_DIRECT_P (flag))
    len = flag;
  else
    len = read_uintval (mrb, ctx);
  aryval = mrb_ary_new_capa (mrb, len);
  register_value (mrb, ctx, aryval);
  ret = mrb_ary_ptr (aryval);
  ptr = ARY_PTR (ret);
  ai = mrb_gc_arena_save (mrb);
  for (i = 0; i < len; i++)
    {
      ptr[i] = read_value (mrb, ctx);
      mrb_gc_arena_restore (mrb, ai);
    }
  ARY_SET_LEN (ret, len);
  if (SER_UFLAG_FROZEN_P (flag))
    MRB_SET_FROZEN_FLAG (ret);
  debug_print_func_end (ctx);
  return ret;
}

static inline void
write_hash (mrb_state * mrb, struct serialize_ctx *ctx, struct RHash *h)
{
  mrb_value keys, *keysptr;
  mrb_int ai, keyslen, i;
  debug_print_func_start (ctx);
  register_value (mrb, ctx, mrb_hash_value (h));
  ai = mrb_gc_arena_save (mrb);
  keys = mrb_hash_keys (mrb, mrb_hash_value (h));
  keysptr = RARRAY_PTR (keys);
  keyslen = RARRAY_LEN (keys);
  if (SER_UFLAG_FR_DIRECT_P (keyslen) && !MRB_FROZEN_P (h))
    write_type (mrb, ctx, SER_TYPE_FLAG (MRB_TT_HASH, keyslen));
  else
    {
      write_type (mrb, ctx,
		  SER_TYPE_FLAG (MRB_TT_HASH,
				 MRB_FROZEN_P (h) ? SER_UFLAG_FROZEN :
				 SER_UFLAG_INDIRECT));
      write_uintval (mrb, ctx, keyslen);
    }
  for (i = 0; i < keyslen; i++)
    {
      write_value (mrb, ctx, keysptr[i]);
      write_value (mrb, ctx,
		   mrb_hash_get (mrb, mrb_hash_value (h), keysptr[i]));
    }
  mrb_gc_arena_restore (mrb, ai);
  debug_print_func_end (ctx);
}

static inline struct RHash *
read_hash (mrb_state * mrb, struct serialize_ctx *ctx, mrb_int flag)
{
  mrb_value hashval;
  mrb_int ai, len, i;
  struct RHash *ret;
  debug_print_func_start (ctx);
  hashval = mrb_hash_new (mrb);
  register_value (mrb, ctx, hashval);
  ret = mrb_hash_ptr (hashval);
  if (SER_UFLAG_FR_DIRECT_P (flag))
    len = flag;
  else
    len = read_uintval (mrb, ctx);
  ai = mrb_gc_arena_save (mrb);
  for (i = 0; i < len; i++)
    {
      mrb_value key, value;
      key = read_value (mrb, ctx);
      value = read_value (mrb, ctx);
      mrb_hash_set (mrb, hashval, key, value);
      mrb_gc_arena_restore (mrb, ai);
    }
  if (SER_UFLAG_FROZEN_P (flag))
    MRB_SET_FROZEN_FLAG (ret);
  debug_print_func_end (ctx);
  return ret;
}

static inline void
write_range (mrb_state * mrb, struct serialize_ctx *ctx, struct RRange *r)
{
  debug_print_func_start (ctx);
  register_value (mrb, ctx, mrb_obj_value (r));
  write_type (mrb, ctx,
	      SER_TYPE_FLAG (r->excl ? MRB_TT_EX_RANGE : MRB_TT_RANGE,
			     MRB_FROZEN_P (r) ? SER_UFLAG_FROZEN :
			     SER_UFLAG_INDIRECT));
  write_value (mrb, ctx, r->edges->beg);
  write_value (mrb, ctx, r->edges->end);
  debug_print_func_end (ctx);
}

static inline struct RRange *
read_range (mrb_state * mrb,
	    struct serialize_ctx *ctx, mrb_int excl, mrb_int flag)
{
  struct RRange *ret;
  debug_print_func_start (ctx);
  ret = (struct RRange *) mrb_obj_alloc (mrb, MRB_TT_RANGE, mrb->range_class);
  ret->edges = NULL;
  ret->excl = 0;
  register_value (mrb, ctx, mrb_range_value (ret));
  ret->edges = (mrb_range_edges *) mrb_malloc (mrb, sizeof (mrb_range_edges));
  ret->edges->beg = read_value (mrb, ctx);
  ret->edges->end = read_value (mrb, ctx);
  ret->excl = excl;
  if (SER_UFLAG_FROZEN_P (flag))
    MRB_SET_FROZEN_FLAG (ret);
  debug_print_func_end (ctx);
  return ret;
}

static inline void
write_value (mrb_state * mrb, struct serialize_ctx *ctx, mrb_value value)
{
  enum mrb_vtype type = mrb_type (value);
  debug_print_func_start (ctx);
  switch (type)
    {

    case MRB_TT_FALSE:
      if (mrb_nil_p (value))
	{
	  write_type (mrb, ctx, MRB_TT_EX_NIL);
	  break;
	}
    case MRB_TT_TRUE:
    case MRB_TT_UNDEF:
      write_type (mrb, ctx, type);
      break;
    case MRB_TT_FIXNUM:
      if (mrb_nil_p (write_ref (mrb, ctx, value)))
	write_fixnum (mrb, ctx, mrb_fixnum (value));
      break;
    case MRB_TT_SYMBOL:
      if (mrb_nil_p (write_ref (mrb, ctx, value)))
	write_symbol (mrb, ctx, mrb_symbol (value));
      break;
    case MRB_TT_FLOAT:
      if (mrb_nil_p (write_ref (mrb, ctx, value)))
	write_float (mrb, ctx, mrb_float (value));
      break;
    case MRB_TT_OBJECT:
      if (mrb_obj_ptr (value) == mrb->top_self)
	{
	  write_type (mrb, ctx, MRB_TT_EX_TOP);
	  break;
	}
      if (mrb_nil_p (write_ref (mrb, ctx, value)))
	write_object (mrb, ctx, mrb_obj_ptr (value));
      break;
    case MRB_TT_EXCEPTION:
      if (mrb_nil_p (write_ref (mrb, ctx, value)))
	write_exception (mrb, ctx, mrb_obj_ptr (value));
      break;
    case MRB_TT_CLASS:
      if (mrb_nil_p (write_ref (mrb, ctx, value)))
	write_class (mrb, ctx, mrb_class_ptr (value));
      break;
    case MRB_TT_MODULE:
      if (mrb_nil_p (write_ref (mrb, ctx, value)))
	write_module (mrb, ctx, mrb_class_ptr (value));
      break;
    case MRB_TT_ENV:
      if (mrb_nil_p (write_ref (mrb, ctx, value)))
	write_env (mrb, ctx, (struct REnv *) mrb_obj_ptr (value));
      break;
    case MRB_TT_PROC:
      if (mrb_nil_p (write_ref (mrb, ctx, value)))
	write_proc (mrb, ctx, mrb_proc_ptr (value));
      break;
    case MRB_TT_ARRAY:
      if (mrb_nil_p (write_ref (mrb, ctx, value)))
	write_array (mrb, ctx, mrb_ary_ptr (value));
      break;
    case MRB_TT_HASH:
      if (mrb_nil_p (write_ref (mrb, ctx, value)))
	write_hash (mrb, ctx, mrb_hash_ptr (value));
      break;
    case MRB_TT_STRING:
      if (mrb_nil_p (write_ref (mrb, ctx, value)))
	write_string (mrb, ctx, mrb_str_ptr (value));
      break;
    case MRB_TT_RANGE:
      if (mrb_nil_p (write_ref (mrb, ctx, value)))
	write_range (mrb, ctx, mrb_range_raw_ptr (value));
      break;
    case MRB_TT_SCLASS:
      if (mrb_nil_p (write_ref (mrb, ctx, value)))
	write_sclass (mrb, ctx, mrb_class_ptr (value));
      break;
    case MRB_TT_FREE:
    case MRB_TT_ICLASS:
    case MRB_TT_CPTR:
    case MRB_TT_FILE:
    case MRB_TT_DATA:
    case MRB_TT_FIBER:
    case MRB_TT_ISTRUCT:
    case MRB_TT_BREAK:
      if (mrb_nil_p (write_ref (mrb, ctx, value)))
	{
	  mrb_value cval, args;
	  mrb_sym mid;
	  struct RClass *c;
	  mrb_method_t m;
	  register_value (mrb, ctx, value);
	  cval = mrb_obj_value (mrb_obj_ptr (value)->c);
	  args = mrb_nil_value ();
	  mid = mrb_intern_lit (mrb, "__serialize");
	  c = mrb_obj_class (mrb, value);
	  m = mrb_method_search_vm (mrb, &c, mid);
	  if (!MRB_METHOD_UNDEF_P (m))
	    args = mrb_funcall_argv (mrb, value, mid, 0, NULL);
	  write_type (mrb, ctx, type);
	  write_value (mrb, ctx, cval);
	  write_value (mrb, ctx, args);
	  break;
	}

    case MRB_TT_MAXDEFINE:
      mrb_assert (0);
    }

  debug_print_func_end (ctx);
}

static inline mrb_value
read_value (mrb_state * mrb, struct serialize_ctx *ctx)
{
  mrb_int type;
  mrb_value ret = mrb_nil_value ();
  debug_print_func_start (ctx);
  type = read_type (mrb, ctx);
  switch (SER_TYPE (type))
    {
    case MRB_TT_EX_NIL:
      ret = mrb_nil_value ();
      break;
    case MRB_TT_FALSE:
      ret = mrb_false_value ();
      break;
    case MRB_TT_TRUE:
      ret = mrb_true_value ();
      break;
    case MRB_TT_UNDEF:
      ret = mrb_undef_value ();
      break;
    case MRB_TT_FIXNUM:
      ret = mrb_fixnum_value (read_fixnum (mrb, ctx, SER_SFLAG (type)));
      break;
    case MRB_TT_SYMBOL:
      ret = mrb_symbol_value (read_symbol (mrb, ctx, SER_UFLAG (type)));
      break;
    case MRB_TT_FLOAT:
      ret = mrb_float_value (mrb, read_float (mrb, ctx));
      break;
    case MRB_TT_EX_TOP:
      ret = mrb_obj_value (mrb->top_self);
      break;
    case MRB_TT_OBJECT:
      ret = mrb_obj_value (read_object (mrb, ctx, SER_UFLAG (type)));
      break;
    case MRB_TT_EXCEPTION:
      ret = mrb_obj_value (read_exception (mrb, ctx, SER_UFLAG (type)));
      break;
    case MRB_TT_CLASS:
    case MRB_TT_MODULE:
      ret = mrb_obj_value (read_class (mrb, ctx));
      break;
    case MRB_TT_ENV:
      ret = mrb_obj_value (read_env (mrb, ctx));
      break;
    case MRB_TT_PROC:
      ret = mrb_obj_value (read_proc (mrb, ctx));
      break;
    case MRB_TT_ARRAY:
      ret = mrb_ary_value (read_array (mrb, ctx, SER_UFLAG (type)));
      break;
    case MRB_TT_HASH:
      ret = mrb_hash_value (read_hash (mrb, ctx, SER_UFLAG (type)));
      break;
    case MRB_TT_STRING:
      ret = mrb_obj_value (read_string (mrb, ctx, SER_UFLAG (type)));
      break;
    case MRB_TT_RANGE:
    case MRB_TT_EX_RANGE:
      ret =
	mrb_obj_value (read_range
		       (mrb, ctx, type == MRB_TT_EX_RANGE, SER_UFLAG (type)));
      break;
    case MRB_TT_EX_REF:
      ret = read_ref (mrb, ctx, SER_SFLAG (type));
      break;
    case MRB_TT_SCLASS:
      ret = mrb_singleton_class (mrb, read_value (mrb, ctx));
      break;
    case MRB_TT_FREE:
    case MRB_TT_CPTR:
    case MRB_TT_ICLASS:
    case MRB_TT_FILE:
    case MRB_TT_DATA:
    case MRB_TT_FIBER:
    case MRB_TT_ISTRUCT:
    case MRB_TT_BREAK:
      {
	static char const *mnames[] = {
	  "__deserialize", "initialize", NULL
	}, **mp;
	mrb_value cval, args;
	struct RClass *c;
	cval = read_value (mrb, ctx);
	args = read_value (mrb, ctx);
	c = mrb_class_ptr (cval);
	ret = mrb_obj_value (mrb_obj_alloc (mrb, SER_TYPE (type), c));
	register_value (mrb, ctx, ret);
	for (mp = mnames; *mp; mp++)
	  {
	    mrb_sym mid;
	    mrb_int argc;
	    mrb_value *argv;
	    mrb_method_t m;
	    mid = mrb_intern_static (mrb, *mp, strlen (*mp));
	    m = mrb_method_search_vm (mrb, &c, mid);
	    if (MRB_METHOD_UNDEF_P (m))
	      continue;
	    if (mrb_nil_p (args))
	      {
		argc = 0;
		argv = NULL;
	      }
	    else if (mrb_array_p (args))
	      {
		argc = RARRAY_LEN (args);
		argv = RARRAY_PTR (args);
	      }
	    else
	      {
		argc = 1;
		argv = &args;
	      }
	    mrb_funcall_argv (mrb, ret, mid, argc, argv);
	    break;
	  }
      }
      break;
    default:
      mrb_raise (mrb, E_RUNTIME_ERROR,
		 "invalid serialize format (unexpected type)");
    }

  if (ctx->flags & SER_DEBUG)
    {
      debug_print_indent (ctx);
      fprintf (stderr, "ret = ");
      debug_print_type (ctx, mrb_type (ret));
    }
  debug_print_func_end (ctx);
  return ret;
}

static inline mrb_int
serialize_parse_flags (mrb_state * mrb)
{
  mrb_value opt;
  mrb_int flags;

  flags = SER_MEM;
  if (mrb_get_args (mrb, "|H", &opt))
    {
      if (mrb_bool
	  (mrb_hash_get
	   (mrb, opt, mrb_symbol_value (mrb_intern_lit (mrb, "debug")))))
	flags |= SER_DEBUG;
      if (mrb_bool
	  (mrb_hash_get
	   (mrb, opt, mrb_symbol_value (mrb_intern_lit (mrb, "stdio")))))
	flags &= ~SER_MEM;
    }
  return flags;
}

static mrb_value
mrb_serialize_serialize (mrb_state * mrb, mrb_value self)
{
  mrb_int flags;
  struct serialize_ctx ctx;
  mrb_value ret;

  flags = serialize_parse_flags (mrb);
  flags |= SER_WRITE;
  open_raw (mrb, &ctx, mrb_ary_new (mrb), NULL, 0, 0, flags, 0);
  write_value (mrb, &ctx, self);
  ret =
    mrb_str_new (mrb, mrb_serialize_ctx_ptr (mrb, &ctx),
		 mrb_serialize_ctx_len (mrb, &ctx));
  close_raw (mrb, &ctx);
  return ret;
}

static mrb_value
mrb_serialize_deserialize (mrb_state * mrb, mrb_value self)
{
  mrb_int flags;
  struct serialize_ctx ctx;
  mrb_value ret;

  flags = serialize_parse_flags (mrb);
  flags &= ~SER_WRITE;
  open_raw (mrb, &ctx, mrb_ary_new (mrb), RSTRING_PTR (self),
	    RSTRING_LEN (self), 0, flags, 0);
  ret = read_value (mrb, &ctx);
  close_raw (mrb, &ctx);
  return ret;
}

void
mrb_mruby_serialize_gem_init (mrb_state * mrb)
{
  mrb_define_method (mrb, mrb->object_class, "serialize",
		     mrb_serialize_serialize, MRB_ARGS_OPT (1));
  mrb_define_method (mrb, mrb->string_class, "deserialize",
		     mrb_serialize_deserialize, MRB_ARGS_OPT (1));
}

void
mrb_mruby_serialize_gem_final (mrb_state * mrb)
{
}
